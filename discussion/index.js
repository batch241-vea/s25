// JSON Objects
/*
	- it stands for JavaScript Object Notation
	- also used in other programming languages
	- core JavasCript has a built in JSON Object that contains methods for parsing JSON objects and converting strings into JavaScript objects
	- JavaScript objects are not to be confused with JSON
	- JSON is used for serializing different data types into bytes
	- Serialization is the process of converting data into a series of bytes for easier transmission/transfer of info
	- a byte is a unit of data that is eight binary digits(1 and 0) that is used to represent a character(letters, numbers, or typographic symbols)
	- syntax:
		{
			"propertyA": "propertyA",
			"propertyB": "propertyB"
		}
		//Stringify = object to json
*/

// JSON
/*{
	"city": "Quezon City",
	"province": "Metro Manila",
	"country": "Philippines"
}*/


// JSON, Array
/*"cities": [
	{"city": "Quezon City", "province": "Metro Manila", "country": "Philippines"},
	{"city": "Manila City", "province": "Metro Manila", "country": "Philippines"},
	{"city": "Makati City", "province": "Metro Manila", "country": "Philippines"},
	]
console.log(cities[0].city)*/


// PARSE: Converting JSON String to object
// JSON.parse()
let batchesJSON = '[{"batchName": "Batch X"}, {"batchName": "Batch Y"}]';
console.log(JSON.parse(batchesJSON));
// console.log(batchesJSON[0].batchName)

let stringifiedObject = `{"name": "Eric", "age": 9, "address": {"city": "Bonifacio Global City", "country": "Philippines"}}`
console.log(JSON.parse(stringifiedObject));



// STRINGIFY: Converts Objects into String(JSON)
// JSON.stringify

let data = {
	name: 'Gabryl',
	age: 61,
	address: {
		city: 'New York',
		country: 'USA'
	}
}
console.log(data);
console.log(typeof data);


let stringData = JSON.stringify(data);
console.log(stringData);
console.log(typeof stringData);